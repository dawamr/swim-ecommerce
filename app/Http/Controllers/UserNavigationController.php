<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserNavigationController extends Controller
{
    public function index(){
        return view('user.index');
    }
    
    public function createControl($name){
    				Artisan::call('make:controller '.$name);
    }
}